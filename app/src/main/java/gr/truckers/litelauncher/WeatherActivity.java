package gr.truckers.litelauncher;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import gr.truckers.litelauncher.weatherData.Channel;
import gr.truckers.litelauncher.weatherData.Item;
import gr.truckers.litelauncher.weatherService.YahooWeatherService;
import gr.truckers.litelauncher.weatherService.weatherServiceCallback;

public class WeatherActivity extends AppCompatActivity implements weatherServiceCallback {

    //variables for weather
    private ImageView weatherIcon;
    private TextView temperature;
    private YahooWeatherService service;
    private ProgressDialog dialog;
    private TextView location;
    private TextView condition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        //weather section
        weatherIcon = (ImageView) findViewById(R.id.weatherIcon);
        temperature = (TextView) findViewById(R.id.temperature);
        location = (TextView) findViewById(R.id.location);
        condition = (TextView) findViewById(R.id.condition);

        service = new YahooWeatherService(this);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.show();

        service.refreshWeather("Athens, GR");
    }

    @Override
    public void serviceSuccess(Channel channel) {
        dialog.hide();

        Item item = channel.getItem();

        int resourceId = getResources().getIdentifier("icon_" + item.getCondition().getCode(), "drawable", getBaseContext().getPackageName());

        //@SuppressWarnings("deprecation")
        //Drawable weatherIconDrawable = getResources().getDrawable(resourceId);

        weatherIcon.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), resourceId));
        temperature.setText(item.getCondition().getTemperature()+"\u00B0"+channel.getUnits().getTemperature());
        condition.setText(item.getCondition().getDescription());
        location.setText(service.getLocation());
    }

    @Override
    public void serviceFailure(Exception exception) {
        dialog.hide();
        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_LONG).show();
    }
}
