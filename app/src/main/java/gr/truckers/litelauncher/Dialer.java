package gr.truckers.litelauncher;


import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Locale;

public class Dialer extends AppCompatActivity {

    String numberToCall = "";
    TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialer);

        /**
         * Buttons for dialpad
         */
        Button one = (Button) findViewById(R.id.one);
        Button two = (Button) findViewById(R.id.two);
        Button three = (Button) findViewById(R.id.three);
        Button four = (Button) findViewById(R.id.four);
        Button five = (Button) findViewById(R.id.five);
        Button six = (Button) findViewById(R.id.six);
        Button seven = (Button) findViewById(R.id.seven);
        Button eight = (Button) findViewById(R.id.eight);
        Button nine = (Button) findViewById(R.id.nine);
        Button zero = (Button) findViewById(R.id.zero);
        Button call = (Button) findViewById(R.id.call);
        Button delete = (Button) findViewById(R.id.delete);
        Button star = (Button) findViewById(R.id.star);
        Button hash = (Button) findViewById(R.id.hash);
        final EditText number = (EditText) findViewById(R.id.number);

        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                // TODO Auto-generated method stub
                if(status == TextToSpeech.SUCCESS){
                    int result=tts.setLanguage(Locale.ENGLISH);
                    if(result==TextToSpeech.LANG_MISSING_DATA ||
                            result==TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("error", "This Language is not supported");
                        Toast.makeText(getApplication(), "Nope", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        //speak();
                    }
                }
                else
                    Log.e("error", "Initilization Failed!");
            }
        });

        /**
         * onclick listener for each dialpad button
         */
        one.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                number.setText(numberToCall+"1");
                numberToCall = number.getText().toString();
                tts.speak("One", TextToSpeech.QUEUE_ADD, null);
            }
        });

        two.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                number.setText(numberToCall+"2");
                numberToCall = number.getText().toString();
                tts.speak("Two", TextToSpeech.QUEUE_ADD, null);
            }
        });
        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number.setText(numberToCall+"3");
                numberToCall = number.getText().toString();
                tts.speak("Three", TextToSpeech.QUEUE_ADD, null);
            }
        });
        four.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                number.setText(numberToCall+"4");
                numberToCall = number.getText().toString();
                tts.speak("Four", TextToSpeech.QUEUE_ADD, null);
            }
        });
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number.setText(numberToCall+"5");
                numberToCall = number.getText().toString();
                tts.speak("Five", TextToSpeech.QUEUE_ADD, null);
            }
        });
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number.setText(numberToCall+"6");
                numberToCall = number.getText().toString();
                tts.speak("Six", TextToSpeech.QUEUE_ADD, null);
            }
        });
        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number.setText(numberToCall + "7");
                numberToCall = number.getText().toString();
                tts.speak("Seven", TextToSpeech.QUEUE_ADD, null);
            }
        });
        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number.setText(numberToCall+"8");
                numberToCall = number.getText().toString();
                tts.speak("Eight", TextToSpeech.QUEUE_ADD, null);
            }
        });
        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number.setText(numberToCall+"9");
                numberToCall = number.getText().toString();
                tts.speak("Nine", TextToSpeech.QUEUE_ADD, null);
            }
        });
        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number.setText(numberToCall+"0");
                numberToCall = number.getText().toString();
                tts.speak("Zero", TextToSpeech.QUEUE_ADD, null);
            }
        });
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calling();
            }
        });

        star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number.setText(numberToCall+"*");
                numberToCall = number.getText().toString();
                tts.speak("Star", TextToSpeech.QUEUE_ADD, null);
            }
        });
        hash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number.setText(numberToCall+"#");
                numberToCall = number.getText().toString();
                tts.speak("Hash", TextToSpeech.QUEUE_ADD, null);
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int length = number.getText().length();
                if (length > 0) {
                    number.getText().delete(length - 1, length);
                    numberToCall = number.getText().toString();
                    tts.speak("Deletion", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        });
    }


    /**
     * method that makes the phonecall
     * @param numberString
     */
    private void performDial(String numberString) {
        if (!numberString.equals("")) {
            Uri number = Uri.parse("tel:" + numberString);
            Intent dial = new Intent(Intent.ACTION_CALL, number);
            startActivity(dial);
        }
    }

    private void calling(){
        HashMap<String, String> myHashAlarm = new HashMap<String, String>();
        myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_ALARM));
        myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "SOME MESSAGE");

        tts.speak("Calling "+numberToCall, TextToSpeech.QUEUE_ADD, myHashAlarm);
        tts.setOnUtteranceCompletedListener(new TextToSpeech.OnUtteranceCompletedListener() {
            @Override
            public void onUtteranceCompleted(String utteranceId) {
                performDial(numberToCall);
            }
        });
    }
}
