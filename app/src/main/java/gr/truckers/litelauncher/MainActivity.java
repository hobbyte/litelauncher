package gr.truckers.litelauncher;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Locale;

import gr.truckers.litelauncher.smsActivities.SMSMainActivity;
import gr.truckers.litelauncher.weatherData.Channel;
import gr.truckers.litelauncher.weatherData.Item;
import gr.truckers.litelauncher.weatherService.YahooWeatherService;
import gr.truckers.litelauncher.weatherService.weatherServiceCallback;

public class MainActivity extends AppCompatActivity implements weatherServiceCallback {

    //boolean for flashlight
    private boolean isFlashOn = false;
    private Camera mCam;


    //variables for weather
    private ImageView weatherIcon;
    private TextView temperature;

    private YahooWeatherService service;
    private ProgressDialog dialog;

    TextToSpeech tts;
    Button dialer, sms, flash, browser, settings, drawer;
    TextClock clock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Context context = this;
        PackageManager pm = context.getPackageManager();

        dialer = (Button) findViewById(R.id.dialer);
        sms = (Button) findViewById(R.id.sms);
        flash = (Button) findViewById(R.id.flashlight);
        browser = (Button) findViewById(R.id.browser);
        settings = (Button) findViewById(R.id.settings);
        drawer = (Button) findViewById(R.id.app_drawer);
        clock = (TextClock) findViewById(R.id.clock);

        //weather section
        weatherIcon = (ImageView) findViewById(R.id.weatherIcon);
        temperature = (TextView) findViewById(R.id.temperature);

        dialer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onLongClick2(v.getId());
                return true;
            }
        });
        sms.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onLongClick2(v.getId());
                return true;
            }
        });
        flash.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onLongClick2(v.getId());
                return true;
            }
        });
        browser.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onLongClick2(v.getId());
                return true;
            }
        });
        settings.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onLongClick2(v.getId());
                return true;
            }
        });
        drawer.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onLongClick2(v.getId());
                return true;
            }
        });
        clock.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onLongClick2(v.getId());
                return true;
            }
        });
        temperature.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onLongClick2(v.getId());
                return true;
            }
        });
        // if device support camera?
        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Log.e("err", "Device has no camera!");
            return;
        }
        else mCam = Camera.open();

        clock.setFormat12Hour("hh:mm"); // remove pm/am from 12-format

        service = new YahooWeatherService(this);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.show();

        //Text-to-Speech initialise
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                // TODO Auto-generated method stub
                if(status == TextToSpeech.SUCCESS){
                    int result=tts.setLanguage(Locale.ENGLISH);
                    if(result==TextToSpeech.LANG_MISSING_DATA ||
                            result==TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("error", "This Language is not supported");
                        Toast.makeText(getApplication(), "Nope", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        //speak();
                    }
                }
                else
                    Log.e("error", "Initilization Failed!");
            }
        });

        service.refreshWeather("Athens, GR");

    }

    public void showApps(View v){
        Intent i = new Intent(this, DrawerActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void dialer(View view) {
        //Intent intent = new Intent(Intent.ACTION_DIAL);
        Intent intent = new Intent(this, Dialer.class);
        startActivity(intent);
    }

    public void SMS(View view) {
        //Intent intent = new Intent(Intent.ACTION_DIAL);
        Intent intent = new Intent(this, SMSMainActivity.class);
        startActivity(intent);
    }

    public void Browser(View view) {
        startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.google.com")));
    }

    public void Settings(View view) {
        startActivityForResult(new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS), 0);
    }

    public void Weather(View view) {
        Intent intent = new Intent(this, WeatherActivity.class);
        startActivity(intent);
    }

    private Item item;
    @Override
    public void serviceSuccess(Channel channel) {
        dialog.hide();

        item = channel.getItem();

        int resourceId = getResources().getIdentifier("icon_" + item.getCondition().getCode(), "drawable", getBaseContext().getPackageName());

        //@SuppressWarnings("deprecation")
        //Drawable weatherIconDrawable = getResources().getDrawable(resourceId);

        weatherIcon.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), resourceId));
        temperature.setText(item.getCondition().getTemperature() + "\u00B0" + channel.getUnits().getTemperature());
    }

    @Override
    public void serviceFailure(Exception exception) {
        dialog.hide();
        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_LONG).show();
    }

    public void Flashlight(View view) {
        SurfaceTexture mPreviewTexture;
        if (isFlashOn) {
            // turn off flash
            Camera.Parameters cam1 = mCam.getParameters();
            cam1.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            mCam.setParameters(cam1);
            isFlashOn = false;

        }
        else {
            // turn on flash
            isFlashOn = true;
            Camera.Parameters p = mCam.getParameters();
            p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            mCam.setParameters(p);
            mPreviewTexture = new SurfaceTexture(0);
            try {
                mCam.setPreviewTexture(mPreviewTexture);
            } catch (IOException ex) {
                // Ignore
            }
            mCam.startPreview();


        }
    }

    public void onLongClick2(int id){
        switch (id) {
            case R.id.dialer: tts.speak("Dialer", TextToSpeech.QUEUE_ADD, null); break;
            case R.id.sms: tts.speak("SMS", TextToSpeech.QUEUE_ADD, null); break;
            case R.id.flashlight: tts.speak("Flashlight", TextToSpeech.QUEUE_ADD, null); break;
            case R.id.browser: tts.speak("Browser", TextToSpeech.QUEUE_ADD, null); break;
            case R.id.settings: tts.speak("Settings", TextToSpeech.QUEUE_ADD, null); break;
            case R.id.app_drawer: tts.speak("App Drawer", TextToSpeech.QUEUE_ADD, null); break;
            case R.id.clock: tts.speak(""+clock.getText(), TextToSpeech.QUEUE_ADD, null); break;
            case R.id.temperature: tts.speak("The weather in "+service.getLocation()+" is "+item.getCondition().getDescription()+" and it feels like "+item.getCondition().getTemperature()+ " degrees celcius.", TextToSpeech.QUEUE_ADD, null); break;

        }
    }
}
