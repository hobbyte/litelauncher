package gr.truckers.litelauncher.weatherData;

import org.json.JSONObject;

public class Item implements JSONPopulator{

    private Condition condition;
    //private Forecast forecast;

    public Condition getCondition() {
        return condition;
    }

    @Override
    public void populate(JSONObject data) {
        condition = new Condition();
        condition.populate(data.optJSONObject("condition"));

        //forecast = new Forecast();
        //forecast.populate(data.optJSONObject("forecast"));
    }
}
