package gr.truckers.litelauncher.weatherData;

import org.json.JSONObject;

public class Channel implements JSONPopulator{

    private Item item;
    private Units units;
    private String title;

    public Item getItem() {
        return item;
    }

    public Units getUnits() {
        return units;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public void populate(JSONObject data){

        units = new Units();
        units.populate(data.optJSONObject("units"));

        item = new Item();
        item.populate(data.optJSONObject("item"));

    }

    public void checkTitle(JSONObject data){
        title = data.optString("title");
    }
}
