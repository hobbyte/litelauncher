package gr.truckers.litelauncher.weatherData;

import org.json.JSONObject;

public interface JSONPopulator {
    void populate(JSONObject data);
}
