package gr.truckers.litelauncher.smsActivities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import gr.truckers.litelauncher.R;

public class SMSMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_main);
    }

    public void goToInbox(View view) {
        Intent intent = new Intent(this, ReceiveSmsActivity.class);
        startActivity(intent);
    }

    public void goToCompose(View view) {
        Intent intent = new Intent(this, SendSmsActivity.class);
        startActivity(intent);
    }

}
