package gr.truckers.litelauncher.smsActivities;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.os.Handler;

import java.util.HashMap;
import java.util.Locale;

import gr.truckers.litelauncher.R;

public class SendSmsActivity extends Activity {

    Button sendSMSBtn;
    EditText toPhoneNumberET;
    EditText smsMessageET;
    TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sms);
        sendSMSBtn = (Button) findViewById(R.id.btnSendSMS);
        toPhoneNumberET = (EditText) findViewById(R.id.editTextPhoneNo);
        smsMessageET = (EditText) findViewById(R.id.editTextSMS);
        sendSMSBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                sendSMS();
            }
        });
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                // TODO Auto-generated method stub
                if(status == TextToSpeech.SUCCESS){
                    int result=tts.setLanguage(Locale.ENGLISH);
                    if(result==TextToSpeech.LANG_MISSING_DATA ||
                            result==TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("error", "This Language is not supported");
                        Toast.makeText(getApplication(), "Nope", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        //speak();
                    }
                }
                else
                    Log.e("error", "Initilization Failed!");
            }
        });
    }

    protected void sendSMS() {
        HashMap<String, String> myHashAlarm = new HashMap<String, String>();
        myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_ALARM));
        myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "SOME MESSAGE2");

        tts.speak("Sending SMS to "+toPhoneNumberET.getText().toString()+". Text is. "+smsMessageET.getText().toString() , TextToSpeech.QUEUE_ADD, myHashAlarm);
        tts.setOnUtteranceCompletedListener(new TextToSpeech.OnUtteranceCompletedListener() {
            @Override
            public void onUtteranceCompleted(String utteranceId) {
                String toPhoneNumber = toPhoneNumberET.getText().toString();
                String smsMessage = smsMessageET.getText().toString();
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(toPhoneNumber, null, smsMessage, null, null);
                   // Toast.makeText(getApplicationContext(), "Επιτυχής αποστολή.",
                     //       Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                 //   Toast.makeText(getApplicationContext(),
                   //         "Αποτυχία αποστολής, ελέγξτε τον αριθμό.",
                     //       Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                returnHome();

            }
        });
    }

    public void returnHome() {
        finish();
    }

    public void goToInbox(View view) {
        finish();
        Intent intent = new Intent(SendSmsActivity.this, ReceiveSmsActivity.class);
        startActivity(intent);
    }
}