package gr.truckers.litelauncher;

import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

public class DrawerActivity extends AppCompatActivity implements OnItemClickListener, OnItemLongClickListener {

    private PackageManager manager;
    private AppDetail[] apps;
    GridView appList;
    DrawerAdapter drawerAdapter;
    TextToSpeech tts;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_drawer);
            appList = (GridView) findViewById(R.id.gridList);
            loadApps();
            drawerAdapter = new DrawerAdapter(this, apps);
            appList.setAdapter(drawerAdapter);
            appList.setOnItemClickListener(this);
            appList.setOnItemLongClickListener(this);

            //Text-to-Speech initialise
            tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    // TODO Auto-generated method stub
                    if(status == TextToSpeech.SUCCESS){
                        int result=tts.setLanguage(Locale.ENGLISH);
                        if(result==TextToSpeech.LANG_MISSING_DATA ||
                                result==TextToSpeech.LANG_NOT_SUPPORTED){
                            Log.e("error", "This Language is not supported");
                            Toast.makeText(getApplication(), "Nope", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            //speak();
                        }
                    }
                    else
                        Log.e("error", "Initilization Failed!");
                }
            });
        }

    //A simple TTS method to speak out loud the app's name
    public void speak(String name){
       tts.speak(name, TextToSpeech.QUEUE_ADD, null);
    }

    //Load all apps with .Launcher activities on apps[]
    private void loadApps(){
        manager = getPackageManager();

        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> allActivities = manager.queryIntentActivities(i, 0);
        apps = new AppDetail[allActivities.size()];
        for(int j = 0; j < allActivities.size(); j++){
            apps[j] = new AppDetail();

            //I'm an idiot, name==label and label==name here!
            apps[j].name = allActivities.get(j).loadLabel(manager).toString();
            apps[j].label = allActivities.get(j).activityInfo.packageName;
            apps[j].icon = allActivities.get(j).loadIcon(manager);
        }
    }

    //On single click, open the app
    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
        Intent i = manager.getLaunchIntentForPackage(apps[pos].name.toString());
        Intent intent = this.getPackageManager().getLaunchIntentForPackage(apps[pos].label);
        if (intent != null) {
            startActivity(intent);
        } else       Toast.makeText(getApplication(), "Something went wrong!", Toast.LENGTH_SHORT).show();

    }

    //On long click, 'SPEAK' the app name
    public boolean onItemLongClick(AdapterView<?> parent, View view, int pos, long id) {
        speak(apps[pos].name.toString());
        return true;
    }
}