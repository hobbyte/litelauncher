package gr.truckers.litelauncher.weatherService;

import gr.truckers.litelauncher.weatherData.Channel;

public interface weatherServiceCallback {
    void serviceSuccess(Channel channel);

    void serviceFailure(Exception exception);
}
