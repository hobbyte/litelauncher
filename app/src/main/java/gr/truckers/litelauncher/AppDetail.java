package gr.truckers.litelauncher;

import android.graphics.drawable.Drawable;

public class AppDetail {
    CharSequence name;
    String label;
    Drawable icon;
}
